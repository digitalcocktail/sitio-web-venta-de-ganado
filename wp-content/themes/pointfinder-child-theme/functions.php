<?php
	register_nav_menus( array(
        'footer-menu' => esc_html__( 'Menú para el píe de pagína' ),
	) );

    add_action( 'wp_enqueue_scripts', 'pfch_theme_enqueue_styles' );
    function pfch_theme_enqueue_styles() {
        wp_enqueue_style( 'child-style',
            get_stylesheet_directory_uri() . '/style.css',
            array()
        );
    }

    function wpb_adding_scripts() {
        wp_enqueue_script('my_script', get_stylesheet_directory_uri() . '/js/customizer.js', array(), '20151215', true);
    }
    add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );

?>